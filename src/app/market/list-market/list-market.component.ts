import { Component, OnInit } from '@angular/core';
import {MarketService} from '../../service/market.service';
import {Market} from '../../entity/market';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-list-market',
  templateUrl: './list-market.component.html',
  styleUrls: ['./list-market.component.css']
})
export class ListMarketComponent implements OnInit {

  markets: Observable<Market[]>;
  constructor(private marketService: MarketService) { }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.markets = this.marketService.getList().pipe(
      map(response => {
        return response.data;
      })
    );
  }

}
